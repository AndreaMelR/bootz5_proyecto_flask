from flask_wtf import FlaskForm
from wtforms import StringField, FloatField, SubmitField
from wtforms.validators import DataRequired, NumberRange


class PurchaseForm(FlaskForm):
    from_currency = StringField('from_currency', validators=[DataRequired()])
    from_quantity = FloatField('from_quantity', validators=[DataRequired(), NumberRange(min=0.1, max=None, message="Debe introducir una cantidad")])
    to_currency = StringField('to_currency', validators=[DataRequired()])
    to_quantity = FloatField('to_quantity', validators=[DataRequired()])
    precio_unitario = FloatField('precio_unitario', validators=[DataRequired()])
    
    submit=SubmitField ('Submit')
