from flask import Flask
import sqlite3


conn = sqlite3.connect('./data/cryptos.db') #conecto
cur = conn.cursor() #creo cursor

lista_monedas = [( '1', 'Euro', 'EUR'),
                ('2', 'Bitcoin','BTC'),
                ('3', 'Ethereum','ETH'),
                ('4', 'Ripple', 'XRP'),
                ('5', 'Litecoin', 'LTC'),
                ('6', 'Bitcoin Cash', 'BCH'),
                ('7', 'Binance Coin', 'BNB'),
                ('8', 'Tether', 'USDT'),
                ('9', 'Eos', 'EOS'),
                ('10', 'Bitcoin', 'BSV'),
                ('11', 'Stellar', 'XLM'),
                ('12', 'Cardano', 'ADA'),
                ('13', 'Tron', 'TRX'),
            ]

cur.executemany('INSERT INTO cryptos VALUES (?, ?, ?)',lista_monedas)

conn.commit()
conn.close()
 